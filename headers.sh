#!/bin/bash

echo
echo Please Enter the RPC URL:
read url
echo
echo Please enter your RPC Port:
read rpc
echo
echo Please enter the range of blocks seperated by ... like 1...1000: 
read blocks
echo
echo Please wait...

for i in ${blocks}
do
line=$(curl -s --data-binary '{"jsonrpc": "2.0", "method": "get_block", "params":{"height":'$i'}}' -H 'content-type: application/json;' http://${url}:${rpc}/json_rpc)
output=$(echo "$line" | jq -r '.result.block_header | (.nonce|tostring) + " " + (.timestamp|tostring) + " " + (.num_txes|tostring) + " " + (.reward|tostring) + " " + (.difficulty|tostring)')
echo $i $output >> noncedist.csv
done
echo
echo Finished!

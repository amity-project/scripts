import requests
import json

# Lets Say hello
print()
print("Welcome to Wallet-API Wallet creator v0.1")
print()

# Get required information

input_values = [] # Create an empty list

HOST=input("Please enter the daemon host: ")
print()

DPORT=int(input("Please enter your daemons port: "))
print()

APORT=input("Please enter your API port: ")
print()

WNAME=input("Now give your wallet a name: ")
print()

WPSWRD=input("Create a password: ")
print()

APSWRD=input("Finally please enter your API Key set when starting wallet-api: ")

# Close wallet if one is open

headers = {
    'X-API-KEY': '' +str(APSWRD)+ '',
    'accept': 'application/json',
}

response = requests.delete('http://' +str(HOST)+ ':' +str(APORT)+ '/wallet', headers=headers)

# Add User Values to initial JSON Object before serialization
user_values = {'daemonPort': DPORT, 'daemonHost': HOST, 'filename': WNAME, 'password': WPSWRD}

# Serialize step one
user_values = json.dumps(user_values)

# Serialize step two
serialized_values = json.loads(user_values)

#Send the request
def req():

    
    
    try:
        headers = {
        'X-API-KEY': '' +str(APSWRD)+ '',
        'accept': 'application/json',
        'Content-Type': 'application/json',
        }

        data = serialized_values

        response = requests.post('http://' +str(HOST)+ ':' +str(APORT)+ '/wallet/create', headers=headers, json=data)
    except requests.exceptions.ConnectionError:
        print()
        print("The daemon is offline or cannot be reached.")
    if response.status_code == 200:
        print("Succesfully created wallet!")
    elif response.status_code == 403:
        print("Something went wrong?!")
    else:
        print(response)
    
req()

print()
